$(document).ready(function () {
  var $hamburger = $(".hamburger");
  var $dropdownContent = $('.dropdown-content');
  $hamburger.on("click", function (e) {
    $hamburger.toggleClass("is-active");
    $dropdownContent.toggleClass("is-active");
    $dropdownContent.slideToggle(300);
  });
});
